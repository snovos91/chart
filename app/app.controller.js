angular.module('chartApp')
    .controller('chartCtrl', ['$scope', 'chartService', function($scope, chartService) {
    $scope.charts = [];

    chartService.getCharts().then(function(response) {
        $scope.charts = response.data;
    });
}]);