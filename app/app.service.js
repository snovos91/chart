angular.module('chartApp')
    .factory('chartService', ['$http', function ($http) {
        return {
            getCharts: function () {
                return $http.get('data.json');
            }
        }
    }]);

