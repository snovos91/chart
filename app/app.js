'use strict';

// Declare app level module which depends on views, and components
angular.module('chartApp', ['chartModule'])
.factory('chartService', function ($http, $q) {
        return {
            getCharts: function () {
              return $http.get('data.json');
            }
        }
    })
.controller('ChartCtrl', ['$scope', 'chartService', function($scope, chartService) {
        $scope.charts = [];

        chartService.getCharts().then(function(response) {
            $scope.charts = response.data;
        });
    }]);


