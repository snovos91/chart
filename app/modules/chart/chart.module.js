angular.module('chartModule', [])
    .constant("chartConfig", {
        chart: {
            options : {
                scaleOverride: true,
                scaleShowLabels: true,
                bezierCurve: false,
                datasetFill: false,
                scaleFontSize: 11,
                scaleFontStyle: "normal",
                scaleFontColor: "#245970",
                pointDotStrokeWidth: 2,
                scaleShowVerticalLines: false,
                showTooltips: false,
                datasetStrokeWidth: 4,
                barStrokeWidth: 1,
                barValueSpacing: 15,
                barDatasetSpacing: 8,
                legendTemplate: '<ul>'
                + '<% for (var i=0; i<datasets.length; i++) { %>'
                + '<li>'
                + '<span style=\"background-color:<%=datasets[i].strokeColor%>\"></span>'
                + '<% if (datasets[i].label) { %><%= datasets[i].label %><% } %>'
                + '</li>'
                + '<% } %>'
                + '</ul>'
            },
            data :  {
                linear: {
                    options: {
                        scaleStepWidth: 500,
                        scaleLabel: "<%='$' + value %>",
                        scaleSteps: 2
                    },
                    data: {
                        "labels": ["", "Week 48", "Week 49", "Week 50", "Week 51", "Week 52"],
                        "datasets": [
                            {
                                "label": "New Comp",
                                "fillColor": "rgba(151,187,205,0.2)",
                                "strokeColor": "rgba(237,110,55,1)",
                                "pointColor": "rgba(237,110,55,1)",
                                "pointStrokeColor": "#fff",
                                "pointHighlightFill": "#fff",
                                "pointHighlightStroke": "rgba(151,187,205,1)"
                            },
                            {
                                "label": "AnalyzerHR",
                                "fillColor": "rgba(220,220,220,0.2)",
                                "strokeColor": "rgba(37,158,1,1)",
                                "pointColor": "rgba(37,158,1,1)",
                                "pointStrokeColor": "#fff",
                                "pointHighlightFill": "#fff",
                                "pointHighlightStroke": "rgba(225,225,225,1)"
                            },
                            {
                                "label": "Question Right",
                                "fillColor": "rgba(151,187,205,0.2)",
                                "strokeColor": "rgba(21,160,200,1)",
                                "pointColor": "rgba(21,160,200,1)",
                                "pointStrokeColor": "#fff",
                                "pointHighlightFill": "#fff",
                                "pointHighlightStroke": "rgba(151,187,205,1)"
                            }

                        ]
                    }
                },
                bar: {
                    options: {
                        scaleStepWidth: 5,
                        scaleSteps: 2,
                        scaleLabel: "<%= value %>",
                        barStrokeWidth : 1
                    },
                    data: {
                        "labels": ["Week 48", "Week 49", "Week 50", "Week 51", "Week 52"],
                        "datasets": [
                            {
                                "label": "Net Comp",
                                "fillColor": "rgba(237,110,55,1)",
                                "strokeColor": "rgba(237,110,55,1)"

                            },
                            {
                                "label": "AnalyzerHR",
                                "fillColor": "rgba(37,158,1,1)",
                                "strokeColor": "rgba(37,158,1,1)"

                            },
                            {
                                "label": "Question Right",
                                "fillColor": "rgba(21,160,200,1)",
                                "strokeColor": "rgba(21,160,200,1)"
                            }
                        ]
                    }
                }
            }
        }
    });
