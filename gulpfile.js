var gulp = require('gulp');
var connect = require('gulp-connect');
var less = require("gulp-less");
var path = require('path');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var libsSrc = [
    'bower_components/angular/angular.min.js',
    'bower_components/Chart.js/Chart.min.js'
];
var jsSrc = [
    'app/app.js', 'app/app.service.js', 'app/app.controller.js',
    'app/modules/chart/chart.module.js',
    'app/modules/**/**/*.js'
];
gulp.task('vendor', function() {
    return gulp.src(libsSrc)
        .pipe(concat('vendor.js'))
        .pipe(uglify())
        .pipe(gulp.dest('app/dist/vendor'));
});
gulp.task('build', function(){
    return gulp.src(jsSrc)
        .pipe(concat('main.js'))
        .pipe(uglify())
        .pipe(gulp.dest('app/dist'));
});
gulp.task('connect', function() {
    connect.server({
        root: 'app',
        livereload: true
    });
});


gulp.task('less', function () {
    return gulp.src('./app/less/*.less')
        .pipe(less({
            paths: [ path.join(__dirname, 'less', 'includes') ]
        }))
        .pipe(gulp.dest('./app/css'));
});

gulp.task('html', function () {
    gulp.src('./app/*.html')
        .pipe(connect.reload());
});

gulp.task('watch', function () {
    gulp.watch(['./app/less/*.less'], ['less']);
});

gulp.task('default', ['connect', 'watch']);