angular.module('chartModule')
    .directive('chart', ['chartConfig', function(chartConfig) {
        return {
            restrict: 'E',
            scope:  {
                data : '=?'
            },
            templateUrl: 'modules/chart/components/chart.html',
            link: function (scope, element, attrs) {
                var chart;
                var ctx = element[0].querySelector('.chart').getContext('2d');
                var legend = element[0].querySelector('.chart-legend');
                var config = chartConfig.chart;
                var data = scope.data.datasets;
                var chartData = config.data[scope.data.type];
                for(var prop in data) {
                    chartData.data.datasets[prop].data = data[prop];
                }
                for(var prop in chartData.options) {
                    config.options[prop] =  chartData.options[prop];
                }

                chart = scope.data.type === 'linear' ?
                    new Chart(ctx).Line(chartData.data, config.options) :
                    new Chart(ctx).Bar(chartData.data, config.options);
                angular.element(legend).append(chart.generateLegend());
            }
        };
    }]);
